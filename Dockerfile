ARG NODE_VERSION=16

FROM node:${NODE_VERSION}-alpine
RUN apk add --no-cache g++ make python3
RUN yarn global add @vue/cli
EXPOSE 3000
WORKDIR /usr/src/